import React, { useState, useEffect } from 'react';
import Popular from './Popular';
import TopRated from './TopRated';
import Upcoming from './Upcoming';
import axios from 'axios';

const Index = () => {

    const [genre, setGenre] = useState([])

    let url = `https://api.themoviedb.org/3/genre/movie/list?api_key=927f2d7f4c8e1f2606022420021e33aa&language=en-US`

    useEffect(() => {
        queryAPI()

    }, [])

    const queryAPI = async () => {
        await axios.get(url)
            .then(res => {
                const objeto = {}
                res.data.genres.forEach(genre => {
                    objeto[genre.id] = genre.name
                });
                setGenre(objeto)
            })
    };

    return (

        <div className="container mt-5">
            <h2 className="text-center bienvenida-index">Toda la informacion sobre el mundo de las Peliculas</h2>
            <Popular
                genre={genre}
            />
            <div className="dropdown-divider"></div>
            <TopRated />
            <div className="dropdown-divider"></div>
            <Upcoming />

        </div>
    );
};

export default Index;