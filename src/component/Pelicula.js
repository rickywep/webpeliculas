import React from 'react';
import DetallePelicula from './DetallePelicula';


const Pelicula = ({ pelicula, genre }) => {

    const { title, poster_path, vote_average, release_date, overview, genre_ids } = pelicula
    let genres
    if (genre) {
        genres = genre_ids.map(genero => genre[genero])
        
    }

    return (
        <div className="col-3 p-4 ">
            <img type="button" data-toggle="modal" data-target="#exampleModal" className="border border-light shadow p-1 bg-light rounded " src={`https://image.tmdb.org/t/p/w154/${poster_path}`} alt=".." />


            <h6 >{title}</h6>
            <h6 className="text-success" > Puntaje: {vote_average}/10</h6>
            <p className="text-white-50">{release_date}</p>
            <DetallePelicula
                title={title}
                overview={overview}
                genres={genres}
            />


        </div>
    );
};

export default Pelicula;